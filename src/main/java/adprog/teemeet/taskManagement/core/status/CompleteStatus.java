package adprog.teemeet.taskManagement.core.status;

public class CompleteStatus implements Status {
    @Override
    public String showStatus() {
        return "Complete";
    }
}
