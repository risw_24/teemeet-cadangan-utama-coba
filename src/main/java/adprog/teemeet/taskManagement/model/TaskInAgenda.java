package adprog.teemeet.taskManagement.model;

import adprog.teemeet.taskManagement.model.TaskLog;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "taskInAgenda")
@Data
@NoArgsConstructor
public class TaskInAgenda {
    @Id
    @Column(name="agendaId")
    private String agendaId;

    @JsonIgnore
    @OneToMany(mappedBy = "tasks", cascade = CascadeType.ALL)
    private List<TaskLog> taskAgenda;

    public TaskInAgenda(String agendaId){
        this.agendaId = agendaId;
    }
}
