package adprog.teemeet.taskManagement.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "grouplog")
@Data
@NoArgsConstructor
public class GroupLog {

    @Id
    @Column(name="kodeGroup",updatable = false, nullable = false)
    private String kodeGroup;

    @Column(name="nama")
    private String nama;

    @ManyToMany(mappedBy = "groups")
    private List<PersonLog> member;
    /*
    @JsonIgnore
    @OneToMany(mappedBy = "task_group", cascade = CascadeType.ALL)
    private List<TaskLog> group_tasks;

     */

    public GroupLog(String nama, String kodeGroup){
        this.nama = nama;
        this.kodeGroup = kodeGroup;
    }
}
