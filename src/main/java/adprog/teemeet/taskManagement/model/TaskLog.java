package adprog.teemeet.taskManagement.model;
import adprog.teemeet.taskManagement.core.status.CompleteStatus;
import adprog.teemeet.taskManagement.core.status.InProgressStatus;
import adprog.teemeet.taskManagement.core.status.IncompleteStatus;
import adprog.teemeet.taskManagement.core.status.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tasklog")
@Data
@NoArgsConstructor
public class TaskLog {

    @Id
    @Column(name="idTask")
    private Long idTask;

    @Column(name="judul")
    private String judulTask;

    @Column(name="percent")
    private int percent;

    @Column(name="status")
    private String status;

    @Column(name="assignne")
    private String assignne;

    @Column(name="agendaId")
    private String agendaId;

    @ManyToOne
    @JoinColumn(name="tasks")
    private TaskInAgenda tasks;

    public TaskLog(Long id,String judulTask, String assignne, String agendaId){
        this.idTask = id;
        this.judulTask = judulTask;
        this.percent = 0;
        this.assignne = assignne;
        this.status = "";
        this.agendaId = agendaId;
    }
}
