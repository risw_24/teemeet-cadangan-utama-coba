package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;

import java.util.List;

public interface GroupService {
    GroupLog createGroup(String namaGroup, String kodeGroup, String numberPerson);
    GroupLog setNamaGroup(String kodeGroup, String namaGroup);
    void deleteGroup(String kodeGroup);
    GroupLog getGroup(String kodeGroup);
}
