package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.repository.GroupRepository;
import adprog.teemeet.taskManagement.repository.PersonRepository;
import adprog.teemeet.taskManagement.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupTaskServiceImpl implements GroupTaskService{
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private GroupService groupService;

    @Autowired
    private PersonService personService;

    @Override
    public Iterable<TaskLog> getListTask(String kodeGroup) {
        /*
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        List<TaskLog> result = group.getGroup_tasks();

         */
        return null;
    }

    @Override
    public GroupLog addTaskGroup(String kodeGroup, TaskLog task) {
        /*
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        List<TaskLog> tasks = group.getGroup_tasks();
        TaskLog tasklog = taskRepository.findByIdTask(task.getIdTask());
        tasks.add(tasklog);
        group.setGroup_tasks(tasks);
        groupRepository.save(group);
        taskRepository.save(task);

         */
        return null;
    }

    @Override
    public void deleteTaskGroup(GroupLog group, TaskLog task) {
        /*
        List<TaskLog> tasks = group.getGroup_tasks();
        tasks.remove(task);

         */
    }
}
