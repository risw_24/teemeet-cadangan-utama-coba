package adprog.teemeet.taskManagement.service;

import adprog.teemeet.agenda.repository.AgendaEventRepository;
import adprog.teemeet.agenda.service.EventService;
import adprog.teemeet.taskManagement.core.status.CompleteStatus;
import adprog.teemeet.taskManagement.core.status.InProgressStatus;
import adprog.teemeet.taskManagement.core.status.IncompleteStatus;
import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.repository.GroupRepository;
import adprog.teemeet.taskManagement.repository.PersonRepository;
import adprog.teemeet.taskManagement.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskServiceImpl implements TaskService{
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private GroupService groupService;

    @Autowired
    private GroupTaskService groupTaskService;

    @Autowired
    private GroupMemberService groupMemberService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private AgendaEventRepository agendaEventRepository;

    @Autowired
    private EventService eventService;

    @Override
    public TaskLog makeTask(String kodeGroup, Long idTask, String judulTask, List<String> number, String name, String description){
        //eventService.createEvent(name,description,null,null,null);
        //Event event = new Event(name,description,null,null);
        //agendaEventRepository.save(event);
        /*
        IncompleteStatus incompleteStatus = new IncompleteStatus();
        List<PersonLog> assignne = addAssignee(number);
        TaskLog tasks = new TaskLog(idTask,judulTask,assignne);
        tasks.setStatus(incompleteStatus.showStatus());

        groupTaskService.addTaskGroup(kodeGroup,tasks);
        groupMemberService.notifyTask(tasks,kodeGroup,number);
        //taskRepository.save(tasks);

         */
        return null;
    }
    @Override
    public void removeTask(Long id, String kodeGroup){
        /*
        TaskLog task = taskRepository.findByIdTask(id);
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        groupTaskService.deleteTaskGroup(group, task);
        if(taskRepository.findByIdTask(id).getAssignne().size()==0) {
            taskRepository.delete(task);
        }else{
            for(PersonLog assignee : task.getAssignne()){
                assignee.getTasks().remove(task);
            }
            taskRepository.delete(task);
        }

         */

    }

    @Override
    public TaskLog updateTask(Long idTask, int percent,
                              String judulTask, List<String> numberPerson) {
        /*
        TaskLog task = taskRepository.findByIdTask(idTask);
        task.setPercent(percent);
        task.setJudulTask(judulTask);
        for(String numberId : numberPerson){
            PersonLog addPerson = personRepository.findByNumber(numberId);
            task.getAssignne().add(addPerson);
            addPerson.getTasks().add(task);
            personRepository.save(addPerson);
        }
        setStatusTask(task,percent);
        taskRepository.save(task);

         */
        return null;
    }

    public void setStatusTask(TaskLog task, int percent){
        CompleteStatus completeStatus = new CompleteStatus();
        IncompleteStatus incompleteStatus = new IncompleteStatus();
        InProgressStatus inProgressStatus = new InProgressStatus();
        if(percent == 0){
            task.setStatus(incompleteStatus.showStatus());
        }else if(percent<100){
            task.setStatus(inProgressStatus.showStatus());
        }else{
            task.setStatus(completeStatus.showStatus());
        }
    }

    @Override
    public List<PersonLog> addAssignee(List<String> number) {
        List<PersonLog> assignne = new ArrayList<PersonLog>();
        for(String numberPerson : number){
            PersonLog person = personRepository.findByNumber(numberPerson);
            assignne.add(person);
        }
        return assignne;
    }

    @Override
    public Iterable<PersonLog> getAssignee(Long id) {
        /*
        TaskLog task = taskRepository.findByIdTask(id);
        return task.getAssignne();

         */
        return null;
    }

    public void moveAssignne(String numberPerson){

    }

    @Override
    public TaskLog getTask(Long id) {
        return taskRepository.findByIdTask(id);
    }
}
