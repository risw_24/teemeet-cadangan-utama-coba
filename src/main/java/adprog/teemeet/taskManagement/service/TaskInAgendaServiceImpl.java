package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.TaskInAgenda;
import adprog.teemeet.taskManagement.repository.TaskInAgendaRepository;
import adprog.teemeet.taskManagement.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskInAgendaServiceImpl implements TaskInAgendaService{
    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskInAgendaRepository taskInAgendaRepository;

    @Autowired
    private TaskInAgendaService taskInAgendaService;

    @Override
    public TaskInAgenda getTaskInAgenda(String agendaId) {
        TaskInAgenda taskInAgenda = taskInAgendaRepository.findByAgendaId(agendaId);
        return taskInAgenda;
    }

    @Override
    public TaskInAgenda create(String agenda) {
        TaskInAgenda taskInAgenda = new TaskInAgenda(agenda);
        taskInAgendaRepository.save(taskInAgenda);
        return taskInAgenda;
    }
}
