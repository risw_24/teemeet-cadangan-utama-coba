package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.repository.GroupRepository;
import adprog.teemeet.taskManagement.repository.PersonRepository;
import adprog.teemeet.taskManagement.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GroupServiceImpl implements GroupService{
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private GroupService groupService;

    @Autowired
    private GroupMemberService groupMemberService;

    @Autowired
    private PersonService personService;

    @Override
    public GroupLog createGroup(String nama, String kodeGroup, String number) {
        GroupLog group = new GroupLog(nama,kodeGroup);
        groupRepository.save(group);
        //personService.addGroupHasJoin(number, kodeGroup);
        groupMemberService.addMember(number, kodeGroup);
        return group;
    }

    @Override
    public GroupLog getGroup(String kodeGroup) {
        return groupRepository.findByKodeGroup(kodeGroup);
    }

    @Override
    public GroupLog setNamaGroup(String kodeGroup, String namaGroup) {
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        group.setNama(namaGroup);
        groupRepository.save(group);
        return group;
    }

    @Override
    public void deleteGroup(String kodeGroup) {
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        groupRepository.delete(group);
    }
}
