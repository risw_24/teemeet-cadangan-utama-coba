package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;

import java.util.List;


public interface PersonService {
    //PersonLog addGroupHasJoin(String number,String kodeGroup);

    Iterable<GroupLog> getGroupJoin(String numberPerson);
    Iterable<TaskLog> getPersonalTask(String numberPerson);

    PersonLog createPerson(String numberPerson, String namaPerson, String rolePerson);
    PersonLog getPerson(String numberPerson);
    PersonLog updatePerson(String numberPerson, String namaPerson, String rolePerson);
    void deletePerson(String number, String kodeGroup);
}
