package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.repository.GroupRepository;
import adprog.teemeet.taskManagement.repository.PersonRepository;
import adprog.teemeet.taskManagement.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class GroupMemberServiceImpl implements GroupMemberService{
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private GroupService groupService;

    @Autowired
    private PersonService personService;

    @Override
    public Iterable<PersonLog> getMember(String kodeGroup) {
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        return group.getMember();
    }

    @Override
    public GroupLog addMember(String number,String kodeGroup) {

        PersonLog person = personRepository.findByNumber(number);
        List<GroupLog> groupJoin = person.getGroups();
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        groupJoin.add(group);
        person.setGroups(groupJoin);
        personRepository.save(person);
        /*
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        List<PersonLog> member = group.getMember();
        member.add(person);
        group.setMember(member);
        personRepository.save(person);
        for(PersonLog orang : member){
            member.remove(orang);
        }

         */
        return group;
    }

    @Override
    public void removeMember(String kodeGroup,String numberPerson) {
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        PersonLog person = personRepository.findByNumber(numberPerson);
        person.getGroups().remove(group);
        List<PersonLog> member = group.getMember();
        member.remove(person);
        group.setMember(member);
        personRepository.save(person);
        groupRepository.save(group);
    }

    @Override
    public void notifyTask(TaskLog tasks, String kodeGroup, List<String> number) {
        /*
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        for(String numberperson : number){
            PersonLog person = personRepository.findByNumber(numberperson);
            if(group.getMember().contains(person)){
                person.getTasks().add(tasks);
                personRepository.save(person);
            }
        }

         */
    }
}
