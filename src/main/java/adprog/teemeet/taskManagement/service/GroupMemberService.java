package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;

import java.util.List;

public interface GroupMemberService {
    Iterable<PersonLog> getMember(String kodeGroup);
    GroupLog addMember(String number, String kodeGroup);
    void removeMember(String kodeGroup,String numberPerson);
    void notifyTask(TaskLog task, String kodeGroup, List<String> number);
}
