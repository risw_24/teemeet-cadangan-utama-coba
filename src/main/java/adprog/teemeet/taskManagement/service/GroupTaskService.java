package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.TaskLog;

public interface GroupTaskService {
    Iterable<TaskLog> getListTask(String kodeGroup);
    GroupLog addTaskGroup(String kodeGroup, TaskLog task);
    void deleteTaskGroup(GroupLog group,TaskLog task);
}
