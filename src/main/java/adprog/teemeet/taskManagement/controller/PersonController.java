package adprog.teemeet.taskManagement.controller;
import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PersonController {
    @Autowired
    private PersonService personService;

    @PostMapping(path = "/createUser",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createPerson(@RequestParam(value="number") String number, @RequestParam(value="nama") String nama, @RequestParam(value="role") String role) {
        return ResponseEntity.ok(personService.createPerson(number, nama, role));
    }

    @GetMapping(path = "/{number}/{kodeGroup}/showUser", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getPerson(@PathVariable(value = "number") String number) {
        PersonLog person = personService.getPerson(number);
        if (person == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(person);
    }

    @PutMapping(path = "/{number}/{kodeGroup}/editUser", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updatePerson(@PathVariable("number")String number,
                                       @RequestParam("namaPerson") String namaPerson,
                                       @RequestParam("rolePerson") String rolePerson) {
        return ResponseEntity.ok(personService.updatePerson(number, namaPerson, rolePerson));
    }

    @DeleteMapping(path = "/{number}/{kodeGroup}/deleteUser", produces = {"application/json"})
    public ResponseEntity deletePerson(@PathVariable(value = "number") String number,@PathVariable(value="kodeGroup") String kodeGroup) {
        personService.deletePerson(number, kodeGroup);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
    /*
    @PostMapping(path = "/joinGroup", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity addGroup(@PathVariable(value = "number") String number, @PathVariable("kodeGroup")String kodeGroup){
        return ResponseEntity.ok(personService.addGroupHasJoin(number, kodeGroup));
    }
     */

    @GetMapping(path = "/{number}/{kodeGroup}/groupJoined",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<GroupLog>> getGroups(@PathVariable(value = "number") String number) {
        return ResponseEntity.ok(personService.getGroupJoin(number));
    }

    @GetMapping(path = "/{number}/{kodeGroup}/listTask",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<TaskLog>> getTasks(@PathVariable(value = "number") String number) {
        return ResponseEntity.ok(personService.getPersonalTask(number));
    }

}
