package adprog.teemeet.taskManagement.controller;
import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "{number}/{kodeGroup}/task")
public class TaskController {
    @Autowired
    private TaskService taskService;

    @PostMapping(path = "/create", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createTask(@PathVariable(value = "kodeGroup") String kodeGroup,
                                     @RequestParam(value="idTask") Long idTask,
                                     @RequestParam(value="judulTask") String judulTask,
                                     @RequestParam(value="assignne") List<String> number,
                                     @RequestParam(value="name") String name,
                                     @RequestParam(value="desc") String desc) {
        return ResponseEntity.ok(taskService.makeTask(kodeGroup, idTask,judulTask, number,name ,desc));
    }

    @GetMapping(path = "/show/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getTask(@PathVariable(value = "id") Long id) {
        TaskLog task = taskService.getTask(id);
        if (task == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(task);
    }

    @DeleteMapping(path = "/delete/{id}", produces = {"application/json"})
    public ResponseEntity deleteTask(@PathVariable(value = "id") Long id,@RequestParam(value="kodeGroup") String kodeGroup) {
        taskService.removeTask(id,kodeGroup);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/{id}/assignee",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<PersonLog>> getAssignee(@PathVariable(value = "id") Long id) {
        return ResponseEntity.ok(taskService.getAssignee(id));
    }

    @PutMapping(path = "/{id}/updateTask", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateTask(@RequestParam(value="number") Long idTask,
                                     @RequestParam(value="percent") int percent,
                                     @RequestParam(value="judulTask") String judulTask,
                                     @RequestParam(value="assignne") List<String> assignne) {
        return ResponseEntity.ok(taskService.updateTask(idTask, percent,judulTask , assignne));
    }
}
