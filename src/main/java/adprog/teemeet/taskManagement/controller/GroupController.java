package adprog.teemeet.taskManagement.controller;
import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.service.GroupMemberService;
import adprog.teemeet.taskManagement.service.GroupService;
import adprog.teemeet.taskManagement.service.GroupTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class GroupController {
    @Autowired
    private GroupService groupService;

    @Autowired
    private GroupMemberService groupMemberService;

    @Autowired
    private GroupTaskService groupTaskService;

    @PostMapping(path = "/createGroup",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createGroup(@RequestParam(value="nama") String nama, @RequestParam(value="kodeGroup") String kodeGroup,
                                      @RequestParam(value="user") String number) {
        return ResponseEntity.ok(groupService.createGroup(nama, kodeGroup, number));
    }

    @GetMapping(path = "/{number}/{kodeGroup}/showGroup", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getGroup(@PathVariable(value = "kodeGroup") String kodeGroup) {
        GroupLog group = groupService.getGroup(kodeGroup);
        if (group == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(group);
    }

    @PutMapping(path = "/{number}/{kodeGroup}/editGroup", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateGroup(@PathVariable(value = "kodeGroup") String kodeGroup, @RequestParam("namaGroup") String namaGroup) {
        return ResponseEntity.ok(groupService.setNamaGroup(kodeGroup,namaGroup));
    }


    @DeleteMapping(path = "/{number}/{kodeGroup}/deleteGroup", produces = {"application/json"})
    public ResponseEntity deleteGroup(@PathVariable(value = "kodeGroup") String kodeGroup) {
        groupService.deleteGroup(kodeGroup);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/{number}/{kodeGroup}/groupTask",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<TaskLog>> getListTasks(@PathVariable(value = "kodeGroup") String kodeGroup) {
        return ResponseEntity.ok(groupTaskService.getListTask(kodeGroup));
    }

    @GetMapping(path = "/{number}/{kodeGroup}/groupMember",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<PersonLog>> getMember(@PathVariable(value = "kodeGroup") String kodeGroup) {
        return ResponseEntity.ok(groupMemberService.getMember(kodeGroup));
    }

    @PostMapping(path = "/{number}/{kodeGroup}/addMember",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity addMember(@PathVariable(value = "kodeGroup") String kodeGroup,@RequestParam("member") String number) {
        return ResponseEntity.ok(groupMemberService.addMember(number, kodeGroup));
    }

    @DeleteMapping(path = "/{number}/{kodeGroup}/deleteMember", produces = {"application/json"})
    public ResponseEntity deleteMember(@PathVariable(value = "kodeGroup") String kodeGroup,
                                       @RequestParam("deleteMember") String numberPerson) {
        groupMemberService.removeMember(kodeGroup,numberPerson);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    
}
