package adprog.teemeet.agenda.service;

import adprog.teemeet.agenda.core.DailyEvents;
import adprog.teemeet.agenda.model.Event;

import java.time.LocalDate;
import java.util.List;

public interface EventService {
    Event createEvent(String agendaId,String name, String desc, String date, String timeStart, String timeEnd);
    List<Event> getEventByYearMonthDay(int year, int month, int day);
    List<DailyEvents> getDayList(int year, int month);
    LocalDate dateFormatter(int year, int month, int day);

}
