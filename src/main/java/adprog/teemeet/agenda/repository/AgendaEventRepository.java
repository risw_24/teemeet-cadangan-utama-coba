package adprog.teemeet.agenda.repository;

import adprog.teemeet.agenda.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgendaEventRepository extends JpaRepository<Event, Integer> {

    @Query(value = "SELECT * FROM Event e where EXTRACT(MONTH FROM date) = ?2 and EXTRACT(YEAR FROM date) = ?1", nativeQuery = true)
    List<Event> getByYearAndMonth(int year, int month);

    @Query(value = "SELECT * FROM Event e where EXTRACT(MONTH FROM date) = ?2 and EXTRACT(YEAR FROM date) = ?1 and EXTRACT(DAY FROM date) = ?3", nativeQuery = true)
    List<Event> getByYearAndMonthAndDay(int year, int month, int day);

    AgendaEventRepository findByAgendaId(int agendaId);
}
