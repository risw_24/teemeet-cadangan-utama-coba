package adprog.teemeet.agenda.controller;

import adprog.teemeet.agenda.repository.AgendaEventRepository;
import adprog.teemeet.agenda.service.EventService;
import adprog.teemeet.taskManagement.model.TaskInAgenda;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.repository.TaskInAgendaRepository;
import adprog.teemeet.taskManagement.service.TaskInAgendaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.List;

@Controller
@RequestMapping(path = "/agenda")
public class AgendaController {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private EventService eventService;

    @Autowired
    private AgendaEventRepository agendaEventRepository;

    @Autowired
    private TaskInAgendaService taskInAgendaService;

    @GetMapping("")
    public String agenda(){
        LocalDateTime now = LocalDateTime.now();
        return "redirect:/agenda/calendar?month=" + now.getMonth().getValue() + "&year=" + now.getYear();
    }

    @GetMapping("/calendar")
    public String agendaLanding(Model model,
                          @RequestParam(value = "month") int month,
                          @RequestParam(value="year") int year){
        model.addAttribute("month", month);
        model.addAttribute("monthStr", Month.of(month));
        model.addAttribute("year", year);
        model.addAttribute("days", eventService.getDayList(year, month));
        return "agenda/agendaCalendar";
    }

    @GetMapping("/calendar/prev")
    public String agendaPrevMonth(@RequestParam(value = "month") int month,
                                  @RequestParam(value="year") int year){
        month -= 1;
        if (month == 0) {
            month = 12;
            year -= 1;
        }
        return "redirect:/agenda/calendar?month=" + month + "&year=" + year;
    }

    @GetMapping("/calendar/next")
    public String agendaNextMonth(@RequestParam(value = "month") int month,
                                  @RequestParam(value="year") int year){
        month += 1;
        if (month == 13) {
            month = 1;
            year += 1;
        }
        return "redirect:/agenda/calendar?month=" + month + "&year=" + year;
    }

    @GetMapping("/events")
    public String dailyEvents(Model model,
                                @RequestParam(value = "month") int month,
                                @RequestParam(value="year") int year,
                                @RequestParam(value="day") int day){
        model.addAttribute("month", month);
        model.addAttribute("monthStr", Month.of(month));
        model.addAttribute("year", year);
        model.addAttribute("day", day);
        model.addAttribute("events", eventService.getEventByYearMonthDay(year, month, day));
        return "agenda/agendaDailyEvents";
    }

    @GetMapping("/events/create")
    public String createEventForm(Model model,
                              @RequestParam(value = "month") int month,
                              @RequestParam(value="year") int year,
                              @RequestParam(value="day") int day){
        model.addAttribute("date", eventService.dateFormatter(year,month,day));
        return "agenda/createEvent";
    }

    @PostMapping(path = "/{agendaId}/create", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createAgenda(@RequestParam(value="name") String name,
                                     @RequestParam(value="desc") String desc,
                                     @RequestParam(value="date") String date,
                                     @RequestParam(value="start") String start,
                                     @RequestParam(value="finish") String finish,
                                     @PathVariable("agendaId") String agendaId) {

        HttpEntity<TaskInAgenda> request = new HttpEntity<>(new TaskInAgenda(agendaId));
        TaskInAgenda taskInAgenda = restTemplate.postForObject("http://localhost:8080/task/create-agenda?agendaId=" + agendaId,request,TaskInAgenda.class);
        return ResponseEntity.ok(eventService.createEvent(agendaId,name ,desc, date, start, finish));
    }

    @PostMapping(path = "/{agendaId}/create-with-task", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createAgendaWithTask(@RequestParam(value="name") String name,
                                     @RequestParam(value="desc") String desc,
                                     @RequestParam(value="date") String date,
                                     @RequestParam(value="start") String start,
                                     @RequestParam(value="finish") String finish,
                                     @PathVariable("agendaId") String agendaId,
                                               @RequestParam(value="idTask") Long idTask,
                                               @RequestParam(value="judulTask") String judulTask,
                                               @RequestParam(value="assignne") String assignne) {

        HttpEntity<TaskLog> request = new HttpEntity<>(new TaskLog(idTask,judulTask,assignne,agendaId));
        TaskLog taskLog = restTemplate.postForObject("http://localhost:8080/task/create?&idTask=" +
                        idTask + "&judulTask="+
                        judulTask +"&assignne="+
                        assignne + "&agendaId=" + agendaId,
                request,TaskLog.class);
        return ResponseEntity.ok(eventService.createEvent(agendaId,name ,desc, date, start, finish));
    }

    @GetMapping(path = "/show-agenda/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getTaskInAgenda(@PathVariable(value = "id") String id) {
        TaskInAgenda taskInAgenda = taskInAgendaService.getTaskInAgenda(id);
        System.out.println(taskInAgenda.getTaskAgenda());
        return ResponseEntity.ok(taskInAgenda);
    }

}